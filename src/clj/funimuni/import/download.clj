#!/usr/bin/env bb

(require '[clojure.string :as str])

(def BASE_URL "https://fec.gov/files/bulk-downloads")
(def table-descriptions
  {:cn "Candidate master"
   :cm "Committee master"
   :pas2 "Contributions from committees to candidates and independent expenditures"
   :weball "All candidates"
   :ccl "Candidate-committee linkages"
   :webl "House and Senate current campaigns"
   :webk "PAC Summary"
   :indiv "Contributions by individuals"
   :oth "Any transaction from one committee to another"
   :oppexp "Operating expenditures"})

(defn prepare-data [output-directory table-name election-year]
  (let [url]))
